#!/usr/bin/env python3

from matrix_client.client import MatrixClient, Room
from matrix_client.api import MatrixRequestError
import time
import json
import re
import sys
import traceback

class Bot:
  def __init__(self, homeserver, username, password, ownerID, adminRoom):
    self.ownerID = ownerID           # owner of the bot
    self.errorReportRoom = adminRoom # room to send error reports to
    self.username = username
    self.client = MatrixClient(homeserver)
    try:
      self.client.login_with_password(self.username, password)
    except MatrixRequestError as e:
      print(e)
      if e.code == 403:
        print("Wrong username or password")
        sys.exit(4)
      else:
        print("Unable to login")
        sys.exit(2)
    except MissingSchema as e:
      print("Bad URL format")
      print(e)
      sys.exit(3)

    self.commandMap = []
    print("DEBUG: created Bot for bot \"{}\" with owner \"{}\"".format(self.username, self.ownerID))

  def main(self):
    self.client.add_invite_listener(self.onInvite)
    rooms = self.client.get_rooms();
    print("Current rooms:")
    for name, room in self.client.get_rooms().items():
      print(" - {}".format(name))
      room.add_listener(self.onEvent)
    if self.client.get_user(self.client.user_id).get_display_name() != self.username:
      self.client.get_user(self.client.user_id).set_display_name(self.username)
    while True:
      try:
        self.client.listen_forever()
      except KeyboardInterrupt:
        raise
      except:
        self.reportMainloopError()

      
  def reportMainloopError(self): 
    print("Caught exception in mainloop! Reporting to bot owner")
    err = traceback.format_exc()
    print(err)
    try:
      #self.client.get_rooms()[self.errorReportRoom].send_text('<strong>Error in mainloop</strong>:<br><pre><code>{}</code></pre>'.format(err))
      txn_id = str(self.client.api.txn_id) + str(int(time.time()*1000))
      self.client.api.txn_id += 1
      self.client.api._send("PUT", "/rooms/{}/send/m.room.message/{}".format(self.errorReportRoom, txn_id), {
          'body':"**Error occurred!**\n\n```\n{}```".format(err),
          'msgtype':"m.text",
          'formatted_body':"<p><strong>Error occurred!</strong></p>\n<pre><code>{}</code><pre>".format(err),
          'format': "org.matrix.custom.html"
          })
    except:
      print("Caught exception while reporting exception!")
      print(traceback.format_exc())

  def onInvite(self, room_id, state):
    self.client.join_room(room_id)
    self.client.get_rooms()[room_id].add_listener(self.onEvent)
    print("{} {}".format(type(state), state))

  def onEvent(self, room, event):
    if event['type'] == "m.room.message" and event['content']['msgtype'] == "m.text":
      if room.room_id == self.errorReportRoom: #admin commands
        leaveCommand = re.match("!leave (.+)", event['content']['body'])
        if leaveCommand:
          self.client.get_rooms()[leaveCommand.group(1)].leave()
      for handler in self.commandMap:
        if event['content']['body'].startswith(handler.command):
          self.client.api._send("PUT", "/rooms/{}/typing/{}".format(room.room_id, self.client.user_id), {'typing': True, 'timeout': 30000}) # hack because the library apparently doesn't do typing events
          try:
            room.send_text(handler.handleCommand(event['content']['body']))
          except KeyboardInterrupt:
            raise
          except:
            self.reportMainloopError()
            room.send_notice("Command failed! The error has been reported to {}.".format(self.ownerID))
          self.client.api._send("PUT", "/rooms/{}/typing/{}".format(room.room_id, self.client.user_id), {'typing': False, 'timeout': 30000})
          return

  def registerCommandHandler(self, handler):
    self.commandMap.append(handler)

class CommandHandler:
  def __init__(self, access):
    self.command = "!command"
    self.accessControl = []

  def handleCommand(self, update):
    return "Default CommandHandler reply"
