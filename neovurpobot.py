#!/usr/bin/env python3

from libvurpobot import *
import time
import sys
import subprocess
import grequests
import json

def isInt(string):
  try:
    int(string)
    return True
  except:
    return False

class HacklabHandler(CommandHandler):
  def __init__(self, access):
    self.command = "!hacklab"
    self.accessControl = access
  
  def handleCommand(self, message):
    apiServer="localhost"
    hacklabRequests = [
        grequests.get(url="http://{}/pi_api/temp/".format(apiServer), params={"a":"getTemp"}),              # 0: temperature
        grequests.get(url="http://{}/pi_api/humidity/".format(apiServer), params={"a":"getHumidity"}),      # 1: humidity
        grequests.get(url="http://{}/pi_api/gpio/".format(apiServer), params={"a":"readPin", "pin":"1"}),   # 2: electronics
        grequests.get(url="http://{}/pi_api/gpio/".format(apiServer), params={"a":"readPin", "pin":"0"}),   # 3: mechanics
        grequests.get(url="http://{}/pi_api/pir/".format(apiServer), params={"a":"getStatus"})]             # 4: PIR
    hacklabResponses = grequests.map(hacklabRequests)
    if None in hacklabResponses:
        raise ConnectionError("A request for the /hacklab command failed!")

    temperature = json.loads(hacklabResponses[0].text)['data']
    humidity = json.loads(hacklabResponses[1].text)['data']
    electronicsLight = json.loads(hacklabResponses[2].text)['data'] == "0"
    mechanicsLight = json.loads(hacklabResponses[3].text)['data'] == "0"
    pirStatus = json.loads(hacklabResponses[4].text)['time']

    if not electronicsLight and not mechanicsLight:
      lightStatus = "Lights are off. Hacklab is probably empty."
    elif electronicsLight != mechanicsLight:
      lightStatus = "Lights are on in the {} room.".format(("electronics" if electronicsLight else "mechanics"))
    else:
      lightStatus = "Lights are on in both rooms!"

    responseMessage = "{} Last movement at {}. Temperature is {:.1f}\u00b0C. Humidity is {}%".format(lightStatus, pirStatus, temperature, humidity)
    return responseMessage

if __name__ == "__main__":
  config = json.load(open("config.json", "r"))
  processor = Bot(config['homeserver'], config['username'], config['password'], config['owner'], config['adminRoom'])
  processor.registerCommandHandler(HacklabHandler([]))
  while True:
    try:
      processor.main()
    except KeyboardInterrupt:
      print("Caught KeyboardInterrupt")
      sys.exit(0)

